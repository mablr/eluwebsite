# eluwebsite

Dépot contenant les sources du site d'Elukerio basé sur le générateur de sites statiques Pelican.

La partie "thème" devrait à terme être complètement séparée.

## Environnement de travail

Pour travailler sur ce projet, il faut avoir un environnement Python.

Il est recommandé de créer un environnement virtuel via `virtualenv`. Donc si c'est pas déjà fait, on exécute :
```
pip install virtualenv
```

On peut maintenant créer un environnement virtuel, puis y entrer :
```
virtualenv ~/virtualenvs/pelican
cd ~/virtualenvs/pelican
source bin/activate
```

Une fois dans l'environnement virtuel, il faut installer pelican (on prend aussi le module markdown) :
```
pip install pelican[Markdown]
```

Bravo, votre environnement de travail est prêt ! Il ne reste plus qu'à cloner le dépot :
```
git clone https://git.elukerio.org/elukerio/eluwebsite
```

Pour consulter en direct vos modifications en local, lancez :
```
pelican --listen --autoreload
```

## Configuration, automatisation et publication

Si vous faites partie des élus ayant leur clef publique sur le serveur web, en gros, vous avez le droit de pousser les mises à jours en prod. Les informations qui vont suivre vont sûrement vous être utiles.

Pelican repose pricipalement sur 3 fichiers de configuration.

### pelicanconf.py
Ce fichier contient les variables globales définies par la personne qui a créé le site web.

Il est utilisé à chaque génération du site par Pelican.

Voici quelques variables importantes.

- **SITENAME** : nom du site web
- **SITEURL** : doit contenir l'URL sur laquelle le site sera disponible, préfixe les liens internes
- **RELATIVE_URLS** : permet d'ignorer la variable précédente lors du dévolopement en local (par défaut : False)
- **PATH** : emplacement du contenu à publier
- **THEME** : emplacement du thème, pour nous c'est `themes/elu`
- **DEFAULT_PAGINATION** : pas encore implémentée dans le thème elu
- **DEFAULT_LANG** : dans notre cas `fr`
- **AUTHOR** : auteur par défaut pour les articles
- **TIMEZONE** : dans notre cas `Europe/Paris`

### publishconf.py
Ce fichier concerne l'étape de publication du site. Il permet d'écraser le contenu de certaines variables déjà définies dans `pelicanconf.py`.
Il est associé à un Makefile généré lors de la configuration initiale avec `pelican-quickstart`.

Voici quelques variables importantes.

- **DELETE_OUTPUT_DIRECTORY** : indique s'il faut effacer le dossier de sortie à chaque génération (par défaut : True)

### tasks.py

Ce fichier contient les différentes fonctions d'automatisation de pubblication.

Voici à quoi ressemble la partie qu'il faudra généralement éditer :
```
SETTINGS_FILE_BASE = 'pelicanconf.py'
SETTINGS = {}
SETTINGS.update(DEFAULT_CONFIG)
LOCAL_SETTINGS = get_settings_from_file(SETTINGS_FILE_BASE)
SETTINGS.update(LOCAL_SETTINGS)
CONFIG = {
    'settings_base': SETTINGS_FILE_BASE,
    'settings_publish': 'publishconf.py',
    # Output path. Can be absolute or relative to tasks.py. Default: 'output'
    'deploy_path': SETTINGS['OUTPUT_PATH'],
    # Remote server configuration
    'ssh_user': 'rms',
    'ssh_host': 'eluwebserv',
    'ssh_port': '22',
    'ssh_path': '/var/www/elukerio',
    # Port for `serve`
    'port': 8000,
}
```
