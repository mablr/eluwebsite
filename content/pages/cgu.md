Title: Conditions Générales D'Utilisation
Date: 2020-04-09 14:24
Slug: cgu
Status: Hidden
---
# Nos engagements
## La vie privée d'abord ...

La vie privée est précieuse, nous tenons à la préserver le plus possible. Pour cela, nous n'utilisons que des logiciels libres ce qui permet de garantir un maximum de transparence, de plus nous mettons en place le chiffrement des données dès que c'est possible techniquement. Nous pouvons ainsi maintenir un niveau de vie privée "raisonnable" pour nos adhérents, mais il faut garder à l'esprit que la donnée numérique la mieux protégée est celle qui n'existe pas !

## ... mais concrètement, on fait quoi ?

D'abord il est important de rappeler que nous excluons de nombreuses pratiques, telles que le pistage des utilisateurs à des fins publicitaires ou la collecte abusive de données de télémétrie. Nous nous engageons sur l'honneur à ne jamais consulter les données privées stockées sur nos serveurs. Nous collectons deux types de données. D'une part, il y a les courriels et autres types de fichiers déposés par les utilisateurs dans le cadre de l'usage de leurs services, ces données sont chiffrées dans la mesure du possible. D'autre part, il y a les informations de connexion (logs) que nous sommes obligés de conserver pendant 15 jours dans le cadre de la législation européenne. Nous ne récoltons jamais plus que nécessaire. Par ailleurs, nous nous conformons à la charte CHATONS.
Vous restez maître à bord !

Il est aussi facile de rejoindre l'association Elukerio que de la quitter, vous pouvez à tout moment demander à récupérer une archive de toutes vos données stockées sur nos serveurs. Vous pouvez également auditer les pratiques d'administration des systèmes sur simple demande, pour vous convaincre que nous faisons toujours de notre mieux pour proposer des services informatiques les plus respectueux et loyaux possibles. Nous restons à votre disposition via notre rubrique de contact.

# Vos engagements
## Responsabilité

Vous êtes intégralement responsables de l'utilisation que vous faites de nos outils et vous vous engagez à respecter scrupuleusement la loi en vigueur. Vous êtes également responsables de vos identifiants de connexion, il convient évidemment de choisir une phrase de passe solide et de la garder en sécurité. Toute utilisation abusive de nos services (spam, publication de contenus illégaux, exploitation non autorisée de vulnérabilités ...) pourra faire l'objet d'une procédure prévue par le réglement intérieur (exclusion, plainte).

## Bienveillance

Lorsque vous entrez en contact avec des membres de l'association, veuillez s'il vous plaît à bien expliciter ce que vous attendez de nous avec calme et mesure, les échanges seront bien plus agréables ainsi !
Identité de l'association

Aucune affiliation à l'association Elukerio n'est autorisée sans autorisation préalable du bureau.
Elukerio n'est pas responsable des publications sur les domaines réservés aux adhérents pour leur usage personnel, sous la forme "*.ext.elukerio.org".
