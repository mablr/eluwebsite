Title: Elukerio
Date: 2020-04-09 14:24
Slug: index
Status: hidden
Btn_text: Rejoignez-nous
Btn_link: mailto:contact+web@elukerio.org
Launch_text: Découvrir nos services
Launch_link: category/services.html
---

Association initiée par trois jeunes lycéens en 2017, proposant aujourd’hui un large éventail de services basés sur des logiciels libres. L'objectif est de reprendre le contrôle de sa vie numérique en misant sur le chiffrement, la transparence et la proximité, tout en restant accessible au plus grand nombre du point de vue financier.
