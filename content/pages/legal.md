Title: Mentions Légales
Date: 2020-04-09 14:24
Slug: legal
Status: Hidden
---

# Mentions Légales
## Éditeur
Elukerio

Association loi 1901 déclarée en sous-préfecture de Montbrison le 3 mars 2018 sous le n° W421004629.  
50 Avenue Jean Monnet - 42330 Saint-Galmier - France

## Hébergeur
Association Rezopole  
16 rue de la Thibaudière - 69007 Lyon - France  
SIRET : 440 148 542 00051  
APE : 6110Z  
Déclaration en préfecture n°W691066265  

## Limitation de responsabilité  

Ce site comporte des informations mises à disposition par Elukerio ou par des liens hypertextes vers d’autres sites web qui ne sont pas administrés par Elukerio. L'association Elukerio ne pourra être tenue responsable des erreurs, des omissions ou des résultats qui pourraient être obtenus grâce l'utilisation des informations fournies.

## Propriété intellectuelle

L'ensemble des contenus produits par Elukerio est mis à disposition du public sous licences libres : CC-BY-SA et GNU GPLv3.

Pour plus d'informations : Contactez nous !

# Informatique et libertés
## Informations collectées

En France, les données personnelles sont protégées par la loi n° 78-17 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal, et le RGPD (Règlement général sur la protection des données).

Elukerio ne collecte aucune donnée sur ses utilisateurs sans leur consentement explicite et éclairé, de plus les informations ne sont jamais collectées si les services peuvent fonctionner correctement sans.

## Droit de rectification

Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant. Pour exercer ce droit l'utilisateur est amené à contacter Elukerio par E-mail en fournissant les informations nécessaires aux rectifications. Les changements seront exécutés dans un délai raisonnable à compter de la date de réception de la requête.

Pour plus d'informations : Contactez nous !
