Title: Nextcloud
Date: 2020-04-11 14:24
Slug: nextcloud
Category: Services
Btn_text: Accéder au Cloud
Btn_link: https://cloud.elukerio.org/
---

Les adhérents d'Elukerio disposent d'un stockage en ligne qui permet de synchroniser et de sauvegarder leurs données sur tous leurs appareils, l'ensemble est chiffré pour garantir la sécurité.
