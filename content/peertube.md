Title: PeerTube
Date: 2020-04-09 14:24
Slug: peertube
Category: Services
Btn_text: Accéder à PeerTube
Btn_link: https://peertube.me/
---

Nous sommes convaincus que la décentralisation peut rendre le web meilleur, c'est pourquoi nous mettons à disposition une solution innovante de partage de vidéos en pair à pair, nommée PeerTube et développée par Framasoft.

*Illustration: David Revoy*
